namespace FlowChecker{

    class ContactPersonModel {

        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Comment { get; set; }

    }
}