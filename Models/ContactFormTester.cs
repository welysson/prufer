using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.WaitHelpers;

namespace FlowChecker
{
    class ContactFormTester
    {
        // TODO: put these constants in a file or database. When we create a screen
        private const string PATH_TO_FIELD_NAME = "//input[@class='wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control']";
        private const string PATH_TO_FIELD_EMAIL = "//input[@class='wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control']";
        private const string PATH_TO_FIELD_PHONE = "//input[@class='wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel form-control']";
        private const string PATH_SITE_URL_CONTACT = "https://nordware.io/contato/";
        private const string PATH_TO_CUSTOMER_TYPE_FIELD = "//input[@value='Sou uma consultoria']";
        private const string PATH_TO_CLICK_BUTTON = "//input[@class='wpcf7-form-control wpcf7-submit btn btn-success']";
        private const string PATH_TO_COMMENT_TEXTAREA = "second-textarea";
        private IWebElement element;

        /*
        * Load the form with standard data
        * TODO: change the source of the data to a database or a file
        * TODO: create a function that maps dinamically the forms of the screen. This could be the v1.0 of Prufer
        */
        public void TestForm(
            string name = "Prufer",
            string email = "prufer@nordware.io",
            string tel = "+55(41)3154-8100",
            string comment = "Prufer is testing if the form is working") 
        {

            ContactPersonModel personModel;

            /*
            * Validates if the variables have values
            * TODO: create a try catch or exception handling to return an error when some variable is empty
            */
            if (
                !String.IsNullOrEmpty(name) &&
                !String.IsNullOrEmpty(email) &&
                !String.IsNullOrEmpty(tel) &&
                !String.IsNullOrEmpty(comment))
            {
                personModel = new ContactPersonModel {
                    Name = name, 
                    Email = email, 
                    Phone = tel, 
                    Comment = comment
                };

                IWebDriver driver = new ChromeDriver();

                // This part is used only to see the system working. In production mode 
                // it will be working in "silent" mode, and there will be no screen showing
                // the interaction of the code
                driver.Navigate().GoToUrl(PATH_SITE_URL_CONTACT);
                driver.Manage().Window.Maximize();

                // This is the first step of the function, and it fills the basic fields of the form
                this.FillFirstStep(driver, personModel);

                /*
                * Here is starts the flow
                * TODO: map and create a code for each flow of the form to test the different scenarios
                */
                driver.FindElement(
                    By.XPath(PATH_TO_CUSTOMER_TYPE_FIELD))
                    .Click();

                TimeSpan tSpan = new TimeSpan(0, 0, 5);
                WebDriverWait wait = new WebDriverWait(driver, tSpan);
                wait.Until(
                    SeleniumExtras.WaitHelpers.ExpectedConditions.VisibilityOfAllElementsLocatedBy(
                        By.Id(PATH_TO_COMMENT_TEXTAREA)));

                element = driver.FindElement(By.Id(PATH_TO_COMMENT_TEXTAREA));
                element.Click();
                element.SendKeys(personModel.Comment);

                driver.FindElement(By.XPath(PATH_TO_CLICK_BUTTON)).Click();
            }
        }

        /* 
        * This function fills the first step of the form. Since the form has multiple flows this part is the first 
        * to be filled to get the next fields, that depending on what the customer selected could go to a different
        * path with different other fields.
        */
        private void FillFirstStep(IWebDriver driver, ContactPersonModel personModel)
        {
            element = driver.FindElement(By.XPath(PATH_TO_FIELD_NAME));
            element.SendKeys(personModel.Name);

            element = driver.FindElement(By.XPath(PATH_TO_FIELD_EMAIL));
            element.SendKeys(personModel.Email);

            element = driver.FindElement(By.XPath(PATH_TO_FIELD_PHONE));
            element.SendKeys(personModel.Phone);
        }
    }
}